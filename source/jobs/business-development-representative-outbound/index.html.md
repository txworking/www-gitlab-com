---
layout: job_page
title: "Business Development Representative - Outbound"
---

You love talking about GitLab to people and no question or challenge is too big or small. You have experience working directly with customers in order to answer questions on getting started with a technical product. Your job is to make sure our customers are successful, from the single-shop development firm all the way up to our Fortune 500 customers, and that everyone gets the appropriate level of support.

## Responsibilities

* Work with our Outbound Business Development Lead to develop best practices on outbound sales activities 
* Demand creation (cold calling), demand response (warm calling), demand management (managed follow up process)
* Discover, navigate, and document an organization’s developer/IT landscape
* Maintain all activity and account research using Salesforce.com
* Work with our demand generation manager to document all processes in the handbook and update as needed.
* Work closely with our developer marketing manager to identify customer stories from all of the conversations you have with our customers.

## Qualifications

* Experience in sales, marketing, or customer service
* Experience with phone and email prospecting
* Experience with CRM and/or marketing automation software is highly preferred
* An understanding of B2B software, Open Source software, and the developer product space 
* Articulate, confident, and reliable
* You share our [values](/handbook/#values), and work in accordance with those values.
* You are obsessed with making customers happy.
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.


